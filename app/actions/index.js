import axios from 'axios';

export const FETCH_POSTS = 'FETCH_POSTS',
             CREATE_POST = 'CREATE_POST',
             FETCH_POST = 'FETCH_POST',
             DELETE_POST = 'DELETE_POST';


const ROOT_URL = 'http://reduxblog.herokuapp.com/api';
const API_KEY = '?key=skf83n57vne0fdsf93';
export const generateSID = () => {
  var d = new Date().getTime();
  if(window.performance && typeof window.performance.now === "function"){
      d += performance.now(); //use high-precision timer if available
  }
  var sid = 'xxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (d + Math.random()*16)%16 | 0;
      d = Math.floor(d/16);
      return (c=='x' ? r : (r&0x3|0x8)).toString(16);
  });
  return sid;
};

const data = [
  {
    id: generateSID(),
    title: 'First Post',
    categories: ['Programming'],
    content: 'This is a post'
  },
  {
    id: generateSID(),
    title: 'Second Post',
    categories: ['Redux'],
    content: 'This is the second post'
  },
  {
    id: generateSID(),
    title: 'Third Post',
    categories: ['React'],
    content: 'This is the third post'
  }
];

export function fetchPosts() {
  const request = axios.get(`${ROOT_URL}/posts${API_KEY}`);
  return {
    type: FETCH_POSTS,
    payload: request
  };
}

export function fetchPost(id) {
  const request = axios.get(`${ROOT_URL}/posts/${id}${API_KEY}`);
  return {
    type: FETCH_POST,
    payload: request
  };
}

export function createPost(post) {
  const request = axios.post(`${ROOT_URL}/posts${API_KEY}`, post);
  return {
    type: CREATE_POST,
    payload: request
  };
}

export function deletePost(id) {
  const request = axios.delete(`${ROOT_URL}/posts/${id}${API_KEY}`);
  return {
    type: DELETE_POST,
    payload: request
  };
}





































/* END */
