import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { createPost } from '../actions/index';
import { bindActionCreators } from 'redux';
import { generateSID } from '../actions/index';

class PostsNew extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.getFormData = this.getFormData.bind(this);
    this.validate = this.validate.bind(this);
    this.state = {
      title_error: 'Please enter a title',
      categories_error: 'Please enter a category',
      content_error: 'Please enter some content',
      isTitle: false,
      isCategory: false,
      isContent: false
    };
  }
  validate() {
    if (document.getElementById('title').value === '') {
      this.setState({isTitle: true});
      return;
    } else if (document.getElementById('categories').value === '') {
      this.setState({isCategory: true});
      return;
    } else if (document.getElementById('content').value === '') {
      this.setState({isContent: true});
      return;
    } else {
      this.setState({
        isTitle: false,
        isCategory: false,
        isContent: false
      });

      this.props.createPost(this.getFormData())
        .then(() => {
          this.clear();
          this.context.router.push('/');
        });
    }
  }
  clear() {
    document.getElementById('title').value = '';
    document.getElementById('categories').value = '';
    document.getElementById('content').value = '';
  }
  getFormData() {
    return {
      id: generateSID(),
      title: document.getElementById('title').value,
      categories: document.getElementById('categories').value,
      content: document.getElementById('content').value
    };
  }
  render() {
    return(
      <div className="container-fluid">
          <form>
            <div className="row">
              <div className="col-xs-8">
                <div className={`form-group ${this.state.isTitle ? 'has-error' : ''}`}>
                  <label>Title</label>
                  <input type="text" className="form-control" id="title" />
                </div>
                <div className="text-help">
                  <span className="error-msg">{ this.state.isTitle ? this.state.title_error : '' }</span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-8">
                <div className={`form-group ${this.state.isCategory ? 'has-error' : ''}`}>
                  <label>Categories</label>
                  <input type="text" className="form-control" id="categories" />
                </div>
                <div className="text-help">
                  <span className="error-msg">{ this.state.isCategory ? this.state.categories_error : '' }</span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-8">
                <div className={`form-group ${this.state.isContent ? 'has-error' : ''}`}>
                  <label>Content</label>
                  <textarea className="form-control" height="500" id="content" />
                </div>
                <div className="text-help">
                  <span className="error-msg">{ this.state.isContent ? this.state.content_error : '' }</span>
                </div>
              </div>
            </div>
            <button
              type="submit"
              className="btn btn-primary"
              onClick={(e) =>{
                this.validate();
                e.preventDefault();}}>
              Submit
            </button>
          </form>
      </div>
    );
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createPost: createPost
  }, dispatch);
}


export default connect(null, mapDispatchToProps)(PostsNew);






























/* END */
