import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPosts } from '../actions';
import { Link } from 'react-router';

class PostsIndex extends Component {
  constructor(props) {
    super(props);

  }
  componentWillMount() {
    this.props.fetchPosts();
  }
  render() {
    return (
      <div className="container-fluid">
        <p>List of posts</p>
        <div className="row">
          <div className="col-xs-6">
            <ul className="list-group">
              {
                this.props.posts.map((post) => (
                <Link key={post.id} to={`posts/${post.id}`}>
                  <li href="#" className="list-group-item">{post.title}</li>
                </Link>
                ))
              }
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchPosts: fetchPosts
  }, dispatch);
}

function mapStateToProps(state) {
  return {
    posts: state.posts.all
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(PostsIndex);
