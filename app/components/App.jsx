import React, { Component } from 'react';
import Hello from './component';
import Navbar from './navbar';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        <Navbar />
        <div className="jumbotron">
          <Hello />
        </div>
        { this.props.children }
      </div>
    );
  }
};









/* */
