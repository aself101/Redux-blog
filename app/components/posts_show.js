import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchPost, deletePost } from '../actions/index';

class PostShow extends Component {
  componentWillMount() {
    this.props.fetchPost(this.props.params.id);
  }
  render() {
    if (!this.props.post) {
      return <div>Loading...</div>;
    }
    const { post } = this.props;
    return (
      <div className="container-fluid">
        <div className="panel panel-primary">
          <div className="panel-heading">
            <h3 className="panel-title">{post.title}</h3>
          </div>
          <div className="panel-body">
            <h6>Categories: { post.categories }</h6>
            <p>{ post.content }</p>
          </div>
        </div>
        <div className="panel-footer">
          <Link to={'/'}>
            <button type="button" className="btn btn-primary">Back</button>
          </Link>
          <button className="btn btn-danger pull-xs-right" onClick={() => this.props.deletePost(this.props.params.id)}>Delete Post</button>  
        </div>
      </div>
    );
  }
};

function mapStateToProps(state) {
  return {
    post: state.posts.post
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchPost: fetchPost,
    deletePost: deletePost
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PostShow);
