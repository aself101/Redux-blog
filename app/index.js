import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, browserHistory, hashHistory } from 'react-router';
import promise from 'redux-promise';

import routes from './routes';
import rootReducer from './reducers/index';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

render(
  <Provider store={createStoreWithMiddleware(rootReducer, window.devToolsExtension && window.devToolsExtension())}>
    <Router history={hashHistory} routes={routes} />
  </Provider>,
  document.getElementById('app')
);
