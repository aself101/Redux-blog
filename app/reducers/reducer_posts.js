import { FETCH_POSTS, CREATE_POST, FETCH_POST } from '../actions/index';


const INITIAL_STATE = { all: [], post: null };

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_POST:
      return { ...state, post: action.payload.data };
    case FETCH_POSTS:
      return { ...state, all: action.payload.data };
    case CREATE_POST:
      return { ...state, all: [...state.all, action.payload], post: action.payload.data };
    default:
      return state;
  }
}

















/* END */
